import LikeView from './LikeView';

/**
 * Downvote Notification Component
 */
export default class DownvoteView extends LikeView {

  static message = 'notification.downVoted';
}