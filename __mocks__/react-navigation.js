export default {

};

export const withNavigation = jest.fn();

export const Header = {
  HEIGHT:80
}

export const StackActions = {
  reset: jest.fn()
}
export const NavigationActions = {
  navigate: jest.fn(),
  setParams: jest.fn(),
}